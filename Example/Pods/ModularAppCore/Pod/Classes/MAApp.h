//
//  MAApp.h
//  Modular App Core
//
//  Created by Wills Ward on 6/24/15.
//  Copyright (c) 2015 Ward Development. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  Used to specify details about the app
 */
@protocol MAApp <NSObject>

/**
 *  The name of the app
 */
@property (nonatomic, readonly) NSString* name;

/**
 *  Invokes an action associated with the app
 */
- (void)invoke;

@end