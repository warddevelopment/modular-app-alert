//
//  MAAlertApp.m
//  Pods
//
//  Created by Wills Ward on 6/24/15.
//
//

#import <UIKit/UIKit.h>
#import "MAAlertApp.h"

@implementation MAAlertApp

- (NSString*)name {
	return @"Alert App";
}

- (void)invoke {
	[[[UIAlertView alloc] initWithTitle:@"Alert!!!" message:@"This is from MAAlertApp.m" delegate:nil cancelButtonTitle:@"Cool!" otherButtonTitles:nil] show];
}

@end
