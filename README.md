# ModularAppAlert

[![CI Status](http://img.shields.io/travis/Wills Ward/ModularAppAlert.svg?style=flat)](https://travis-ci.org/Wills Ward/ModularAppAlert)
[![Version](https://img.shields.io/cocoapods/v/ModularAppAlert.svg?style=flat)](http://cocoapods.org/pods/ModularAppAlert)
[![License](https://img.shields.io/cocoapods/l/ModularAppAlert.svg?style=flat)](http://cocoapods.org/pods/ModularAppAlert)
[![Platform](https://img.shields.io/cocoapods/p/ModularAppAlert.svg?style=flat)](http://cocoapods.org/pods/ModularAppAlert)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ModularAppAlert is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "ModularAppAlert"
```

## Author

Wills Ward, wward@warddevelopment.co

## License

ModularAppAlert is available under the MIT license. See the LICENSE file for more info.
