#
# Be sure to run `pod lib lint ModularAppAlert.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "ModularAppAlert"
  s.version          = "1.0.0"
  s.summary          = "A demo of the modular app concept. This app will display an alert view when invoked."
  s.homepage         = "http://stash.warddevelopment.com/projects/COC/repos/modular-app-alert/browse"
  s.license          = 'MIT'
  s.author           = { "Wills Ward" => "wward@warddevelopment.co." }
  s.source           = { :git => "http://willseward@stash.warddevelopment.com/scm/coc/modular-app-alert.git", :tag => s.version.to_s }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'

  s.dependency 'ModularAppCore'
end
